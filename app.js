var meuApp = angular.module('app', ['ngRoute', 'ui.mask', 'base64', 'ngCookies']);
meuApp.config(function ($routeProvider) {

    $routeProvider
    .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'loginCtrl'
    })
    .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'homeCtrl'
    })
    .when('/dadoscadastrais', {
        templateUrl: 'views/dadoscadastrais.html',
        controller: 'dadoscadastraisCtrl'
    })
    .when('/primeiroacesso', {
        templateUrl: 'views/primeiroacesso.html',
        controller: 'dadoscadastraisCtrl'
    })
    .when('/noticias', {
        templateUrl: 'views/noticias.html',
        controller: 'noticiasCtrl'
    })
    .when('/usuarios', {
        templateUrl: 'views/usuarios.html',
        controller: 'usuariosCtrl'
    })
    .when('/pets', {
        templateUrl: 'views/pets.html',
        controller: 'petsCtrl'
    })
    .when('/parceiros', {
        templateUrl: 'views/parceiros.html',
        controller: 'parceirosCtrl'

    })
    .otherwise({ redirectTo: '/login'})

});
