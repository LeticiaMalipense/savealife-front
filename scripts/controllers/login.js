angular.module('app').controller('loginCtrl', function($scope, $http, $location, $base64, $cookieStore){
url = 'http://localhost:9080/savealife/login/';
$scope.usuario = {};

$scope.submitForm = function () {
  data = $scope.usuario;
  var encodedString = $base64.encode(data.email.concat("/").concat(data.senha));
  $http.get(url.concat(encodedString)).then(function (response) {
    if(response.data != ""){
      $cookieStore.put("instituicao", response.data);
      $location.path('/home');
    }else{
      $scope.mensagem = "Usuário ou senha incorretos";
      $("#divMensagem").fadeIn();
    }
  }, function(error) {
    $scope.mensagem = "Usuário ou senha incorretos";
    $("#divMensagem").fadeIn();
  });
  $scope.removerMensagem();
};

$scope.removerMensagem = function(){
  setTimeout(function(){
     $("#divMensagem").fadeOut();
   }, 2000);
 }

});
