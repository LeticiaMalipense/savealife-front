angular.module('app').controller('dadoscadastraisCtrl', function($scope, $http, $location, $cookieStore){
  $('#timepicker1').timepicker("setTime", "");
  $('#timepicker2').timepicker("setTime", "");

  url = 'http://localhost:9080/savealife/instituicao';
  urlUF = 'http://localhost:9080/savealife/estado';
  urlCidade = 'http://localhost:9080/savealife/cidade/';

  $scope.listTelefones = [];

  $scope.voltar = function(){
    $location.path('/');
  };

  $scope.getInstituicao = function(){
      $http.get(url.concat("/").concat(instituicao.id)).then(function (d) {
          $scope.listNoticias = d.data;
        }, function(err) {
          $scope.listNoticias = null;
        });
  }

  $scope.submitForm = function(){
    data = $scope.formdata;
    $http.post(url, data).then(function (d) {
        if(data.cod == null){
            $scope.mensagem = "Dados inseridos com sucesso";
            $scope.removerMensagem();
        }else{
            $scope.mensagem = "Dados alterados com sucesso";
        }
        $scope.formdata = angular.copy({});
        $location.path('/');
      }, function(err) {
        console.log(err);
      });
  };

  $scope.getUF = function(){
    $http.get(urlUF).then(function (d) {
        $scope.listUf = d.data;
      }, function(err) {
        $scope.listUf = null;
      });
  };

  $scope.getCidade = function(estado){
    $http.get(urlCidade.concat(JSON.parse(estado).id)).then(function (d) {
        $scope.listCidade = d.data;
      }, function(err) {
        $scope.listCidade = null;
      });
  };

  $scope.adicionarTelefones = function(telefone){
    if(telefone != null && telefone.ddd != null && telefone.numero != null){
      $scope.listTelefones.push(angular.copy(telefone));
      $scope.telefone = "";
      $scope.mensagemTelefone = "";
    }else{
      $scope.mensagemTelefone = "Campos obrigatórios não informados";
      $("#divMensagem").fadeIn();
    }
    $scope.removerMensagem();
  }

  $scope.removerMensagem = function(){
    setTimeout(function(){
       $("#divMensagem").fadeOut();
     }, 2000);
   }

  $scope.getUF();

});
