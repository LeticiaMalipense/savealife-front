angular.module('app').controller('noticiasCtrl', function ($scope,  $http, $cookieStore) {
  url = 'http://localhost:9080/savealife/noticia';
  instituicao = $cookieStore.get("instituicao");

  $scope.submitForm = function () {
    data = $scope.formdata;
    data.codInstituicao = instituicao.id;
    $http.post(url, data).then(function (d) {
        if(data.cod == null){
            $scope.mensagem = "Dados inseridos com sucesso";
            $scope.removerMensagem();
        }else{
            $scope.mensagem = "Dados alterados com sucesso";
        }
        $scope.formdata = angular.copy({});
        $scope.getNoticias();
      }, function(err) {
        console.log(err);
      });
  };

$scope.editar = function(noticiaSelecionada){
    $scope.formdata = angular.copy(noticiaSelecionada);
}

$scope.excluir = function(noticiaSelecionada){
    $http.get(url.concat("/remove/").concat(noticiaSelecionada.cod)).then(function (d) {
        $scope.mensagem = "Dados excluidos com sucesso";
        $scope.getNoticias();
      }, function(err) {
        console.log(err);
      });
}

$scope.getNoticias = function(){
    $http.get(url.concat("/").concat(instituicao.id)).then(function (d) {
        $scope.listNoticias = d.data;
      }, function(err) {
        $scope.listNoticias = null;
      });
}

$scope.removerMensagem = function(){
  setTimeout(function(){
     $("#divMensagem").fadeOut();
   }, 2000);
 }

  $scope.getNoticias();

});
