angular.module('app').controller('parceirosCtrl', function($scope,  $location, $http){

  url = 'http://localhost:9080/savealife/parceiro';

  $scope.submitForm = function () {
    data = $scope.formdata;
    $http.post(url, data).then(function (d) {
        if(data.cod == null){
            $scope.mensagem = "Dados inseridos com sucesso";
            $scope.removerMensagem();
        }else{
            $scope.mensagem = "Dados alterados com sucesso";
        }
        $scope.formdata = angular.copy({});
        $scope.getParceiros();
      }, function(err) {
        console.log(err);
      });
  };

  $scope.editar = function(parceiroSelecionado){
    $scope.formdata = angular.copy(parceiroSelecionado);
  }

  $scope.excluir = function(parceiroSelecionado){
    $http.get(url.concat("/remove/").concat(parceiroSelecionado.id)).then(function (d) {
        $scope.mensagem = "Dados excluidos com sucesso";
        $scope.getParceiros();
      }, function(err) {
        console.log(err);
      });
  }

  $scope.getParceiros = function(){
    $http.get(url).then(function (d) {
        $scope.listParceiro = d.data;
      }, function(err) {
        $scope.listParceiro = null;
      });
  }

  $scope.getParceirosFilter = function(filter){
    $http.post(url.concat("/filter"), filter).then(function (d) {
        $scope.listParceiro = d.data;
      }, function(err) {
        $scope.listParceiro = null;
      });
  }

  $scope.removerMensagem = function(){
  setTimeout(function(){
     $("#divMensagem").fadeOut();
   }, 2000);
  }

  $scope.getParceiros();


})
